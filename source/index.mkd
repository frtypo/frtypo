---
title: L'espaceur
---

# Prenons un peu d'espace<em class="fa fa-arrows-h"></em>!

La langue française a bien des qualités, mais elle s'est aussi attachée nombre
de règles parfois conservées par galanterie.

Ainsi l'affichage, l'impression visuelle, le style, sont autant de gages de
qualité, de marqueurs pour différencier ceux qui savent des autres. À la
manière des règles d'orthographe que peu maitrisent, des règles de grammaire
inusitées ou encore des expressions qui font tiquer «ceux qui savent».

Dans les règles de l'art, en tous cas celles utilisées par ces typographes qui
gravaient eux même leurs lettres, ils jouaient sur les espaces afin d'ajuster,
justifier, présenter leurs lignes de caractères. Et en général les ponctuations
*dites* hautes avaient une espace fine insérée juste devant.

Cette habitude, ce goût esthétique, a été abandonné dans le monde avec
l'avènement de l'informatique, mais pas en France, où les connaisseurs aiment à
suivre cette règle, se référant de l'imprimerie nationale et de ses pratiques.

Et aujourd'hui ces connaisseurs en arrivent à jongler avec les balises et les
bidouilles, parfois même du code html, afin d'insérer ces quelques pixels qui
font la différence.

## Horreur!

D'autres, limités par leurs outils ou leurs connaissances, insèrent un simple
espace, qui est le pire compromis que l'on puisse imaginer, aboutissant par
exemple à ce genre de chose:

> Quel horrible placement de ponctuation  
> !

## Le compromis technique

Les espaces sont une nécessité pratique, pour l'auteur et le lecteur... Avec
lesquels l'imprimeur joue, les ajoutant ou les enlevant, pour rendre plus
esthétique sa ligne de caractères.

Mais cet imprimeur et cette tradition, ont été pris d'affection par des
générations d'amateurs français, qui s'enorgeuillent de pouvoir reproduire
ces placements faits manuellements, et en viennent à agir souvent au niveau
même des textes.

Et c'est ainsi que pour le web on en arrive à y insérer des **`&nbsp;`**,
**`&thinsp;`** et autres **`&#160;`**.

## Exemples, et tests

Rajoute moi un quart de cadratin mon petit!!!

Joli pourcentage à 19.6% avec un léger espace avant le signe %.

Normalement, une URL ne devrait pas être modifiée, voyons voir: http://april.org.

De la même manière un % au milieu de chiffres devrait rester inchangé: 42%3,
alors que 42% c'est autre chose, non?

## Comment

Intégrez dans votre page 2 règles css et quelques lignes de javascript. Ou
encore les fichiers `style.css` et `script.js` fournis par ce projet.

La seule dépendance externe est [`jquery`](http://jquery.com).

Le code source de ce projet est accessible sur [gitorious](https://gitorious.org/frtypo).

## License

Parce que c'est la meilleure de toutes, nous préférons utiliser la license
[AGPL3+](http://www.gnu.org/licenses/agpl-3.0.html) (version 3 ou plus).

## Références

* [Espaces et signes de ponctuation haute](http://fvsch.com/code/espaces-ponctuation/)
* [Ponctuation haute et Internet](http://typofute.com/ponctuation_haute_et_internet)
* [Ponctuation d'après wikipédia](https://fr.wikipedia.org/wiki/Ponctuation)
